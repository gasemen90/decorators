package ru.sam.decorator;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class DecoratorInterceptor implements MethodInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(DecoratorInterceptor.class);

    private static final String DECORATOR_REQUIRED_MSG = "One of [Decorator, DecoratorWrapper] required";

    private final Map<Method, Function<Function<Object[], ?>, Function<Object[], ?>>> cache = new ConcurrentHashMap<>();

    private static boolean isDefaultDecorator(Class<?> decoratorClass) {
        return DefaultDecorator.class.equals(decoratorClass);
    }

    private static boolean isDefaultDecoratorWrapper(Class<?> decoratorClass) {
        return DefaultDecoratorWrapper.class.equals(decoratorClass);
    }

    @SuppressWarnings("unchecked")
    private static <R> Function<Object[], R> wrap(MethodInvocation invocation) {
        return (Object[] args) -> {
            for (int i = 0; i < args.length; i++) {
                invocation.getArguments()[i] = args[i];
            }
            try {
                return (R) invocation.proceed();
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        };
    }

    public Object invoke(final MethodInvocation invocation) throws Throwable {
        final Function<Object[], ?> original = wrap(invocation);

        if (cache.containsKey(invocation.getMethod())) {
            final Function<Object[], ?> func = cache.get(invocation.getMethod()).apply(original);
            return func.apply(invocation.getArguments());
        }

        final Decorate[] decorators = invocation.getMethod().getAnnotationsByType(Decorate.class);
        if (decorators.length == 0) {
            return invocation.proceed();
        }
        Decorate annotation = decorators[0];
        Class<? extends Function<Function<Object[], ?>, Function<Object[], ?>>> decoratorClass = annotation.value();
        Class<? extends Function<String[],
                ? extends Function<Function<Object[], ?>, Function<Object[], ?>>>> decoratorWrapperClass = annotation.wrapBy();
        Function<Function<Object[], ?>, Function<Object[], ?>> decorator;
        if (!isDefaultDecorator(decoratorClass)) {
            decorator = decoratorClass.newInstance();
        } else if (!isDefaultDecoratorWrapper(decoratorWrapperClass)) {
            decorator = decoratorWrapperClass.newInstance().apply(annotation.withArgs());
        } else {
            logger.error(DECORATOR_REQUIRED_MSG);
            throw new IllegalArgumentException(DECORATOR_REQUIRED_MSG);
        }
        for (int i = 1; i < decorators.length; i++) {
            annotation = decorators[i];
            decoratorClass = annotation.value();
            decoratorWrapperClass = annotation.wrapBy();
            if (!isDefaultDecorator(decoratorClass)) {
                decorator = decorator.compose(decoratorClass.newInstance());
            } else if (!isDefaultDecoratorWrapper(decoratorWrapperClass)) {
                decorator = decorator.compose(decoratorWrapperClass.newInstance().apply(annotation.withArgs()));
            } else {
                logger.error(DECORATOR_REQUIRED_MSG);
                throw new IllegalArgumentException(DECORATOR_REQUIRED_MSG);
            }
        }
        cache.put(invocation.getMethod(), decorator);
        return decorator.apply(original).apply(invocation.getArguments());
    }
}
