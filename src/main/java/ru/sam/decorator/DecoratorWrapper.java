package ru.sam.decorator;

import java.util.function.Function;

public interface DecoratorWrapper<R> extends Function<String[], Decorator<R>> {
    // empty
}
