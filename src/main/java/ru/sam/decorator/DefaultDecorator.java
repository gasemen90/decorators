package ru.sam.decorator;

import java.util.function.Function;

final class DefaultDecorator implements Decorator<Void> {

    private DefaultDecorator() {
        // empty
    }

    @Override
    public Function<Object[], Void> apply(Function<Object[], Void> voidFunction) {
        throw new UnsupportedOperationException();
    }
}
