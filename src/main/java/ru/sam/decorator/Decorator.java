package ru.sam.decorator;

import java.util.function.Function;

public interface Decorator<R> extends Function<Function<Object[], R>, Function<Object[], R>> {
    // empty
}
