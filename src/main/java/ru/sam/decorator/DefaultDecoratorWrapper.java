package ru.sam.decorator;

final class DefaultDecoratorWrapper implements DecoratorWrapper<Void> {

    private DefaultDecoratorWrapper() {
        // empty
    }

    @Override
    public Decorator<Void> apply(String[] strings) {
        throw new UnsupportedOperationException();
    }
}
