package ru.sam.decorator;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DecoratorTest {

    private static final String PHRASE = "Hi! My name is..";

    @Test
    public void decoratorTest() {
        final Injector injector = Guice.createInjector(new Module());
        final HelloClass clazz = injector.getInstance(HelloClass.class);

        assertEquals(clazz.hello(PHRASE), String.format("<wrap><i><b>%s Slim Shady</b></i></wrap>", PHRASE));
    }

    static class Module extends BaseModule {

        @Override
        protected void setup() {
            // empty
        }
    }

}
