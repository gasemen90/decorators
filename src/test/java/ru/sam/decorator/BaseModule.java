package ru.sam.decorator;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import org.aopalliance.intercept.MethodInterceptor;

public abstract class BaseModule extends AbstractModule {

    protected final void configure() {
        final MethodInterceptor decoratorInterceptor = new DecoratorInterceptor();
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Decorators.class), decoratorInterceptor);
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Decorate.class), decoratorInterceptor);

        setup();
    }

    protected abstract void setup();
}
