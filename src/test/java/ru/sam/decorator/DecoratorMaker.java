package ru.sam.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.function.Function;

public class DecoratorMaker implements DecoratorWrapper<String> {
    private static final Logger logger = LoggerFactory.getLogger(DecoratorMaker.class);

    @Override
    public Decorator<String> apply(String[] strings) {
        logger.debug("invoked with args: {}", Arrays.toString(strings));
        return (Function<Object[], String> func) ->
            (Object[] args) -> {
                logger.debug("Wrapped function args: {}", args[0]);
                return String.format("<wrap>%s</wrap>", func.apply(args));
            };
    }
}
