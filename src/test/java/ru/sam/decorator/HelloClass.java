package ru.sam.decorator;

public class HelloClass {

    @Decorate(wrapBy = DecoratorMaker.class, withArgs = {"one", "two", "three"})
    @Decorate(MakeItalic.class)
    @Decorate(MakeBold.class)
    @Decorate(SlimShader.class)
    public String hello(String name) {
        return name;
    }

}
