package ru.sam.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class SlimShader implements Decorator<String> {
    private static final Logger logger = LoggerFactory.getLogger(SlimShader.class);

    public Function<Object[], String> apply(Function<Object[], String> function) {
        logger.debug("invoked");
        return (Object[] args) -> {
            logger.debug("Args: {}", args[0]);
            return String.format("%s Slim Shady", function.apply(args));
        };
    }
}
