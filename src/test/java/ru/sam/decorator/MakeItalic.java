package ru.sam.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class MakeItalic implements Decorator<String> {
    private static final Logger logger = LoggerFactory.getLogger(MakeItalic.class);

    public Function<Object[], String> apply(Function<Object[], String> function) {
        logger.debug("invoked");
        return (Object[] args) -> {
            logger.debug("Args: {}", args[0]);
            return "<i>" + function.apply(args) + "</i>";
        };
    }
}
